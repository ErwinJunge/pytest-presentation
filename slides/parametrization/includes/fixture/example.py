@pytest.fixture(params=[1, 2])
def square(request):
    yield request.param ** 2

def test_sqrt(square):
    root = sqrt(square)
    assert root - int(root) < 0.00001
# 2 passed
