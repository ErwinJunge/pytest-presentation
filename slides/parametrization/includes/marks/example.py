@pytest.fixture
def square(request):
    marker = request.node.get_closest_marker("square_data")
    return marker.args[0] ** 2 if marker else 0


@pytest.mark.square_data(7)
def test_sqrt(square):
    assert square == 49
# 1 passed
