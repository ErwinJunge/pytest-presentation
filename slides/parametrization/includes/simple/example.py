@pytest.mark.parametrize(
    ["value", "expectation"],
    [
        pytest.param(
            1,  # value
            1,  # expectation
            id="1"  # optional
        ),
        (2, 4),  # shorter
        (3, 8),  # FAILED test_tmp.py::test_square[3-8] - assert (3 ** 2) == 8
    ]
)
def test_square(value, expectation):
    assert value ** 2 == expectation
# 1 failed, 2 passed
