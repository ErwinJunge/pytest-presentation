@pytest.mark.parametrize(
    ["value", "expectation"],
    [
        (1, 1),  # PASS
        (2, 4),  # PASS
        (3, 8),  # FAILED 
    ]
)
class TestStuff:
    def test_square(self, value, expectation):
        assert value ** 2 == expectation

    def test_mult(self, value, expectation):
        assert value * value == expectation
# 2 failed, 4 passed
