@pytest.fixture(scope="session")
def sshd_service(docker_ip, docker_services):
    port = docker_services.port_for("sshd", 22)
    docker_services.wait_until_responsive(
        timeout=30.0,
        pause=0.1,
        check=lambda: sshd_is_responsive(hostname=docker_ip, port=port),
    )
    yield Namespace(hostname=docker_ip, port=port)

def test_tampered(..., sshd_service, ...):
    ...
    with ssh_connection(sshd_service.hostname, sshd_service.port) as connection:
        ...
