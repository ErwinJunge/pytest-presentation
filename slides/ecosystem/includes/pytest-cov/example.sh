pytest --cov=tamperbot
<snip>
---------- coverage: platform linux, python 3.10.12-final-0 ----------
Name                     Stmts   Miss  Cover
--------------------------------------------
tamperbot/__init__.py        2      0   100%
tamperbot/__main__.py       21      8    62%
tamperbot/opsgenie.py       10      0   100%
tamperbot/watchlist.py     126      4    97%
--------------------------------------------
TOTAL                      159     12    92%
<snip>
