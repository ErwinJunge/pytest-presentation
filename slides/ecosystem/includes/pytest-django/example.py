from pytest_django.asserts import assertInHTML

# This does not raise, note the order of attributes
assertInHTML(
    '<img src="mypic.jpg" width=100>',
    '<div><img width=100 src="mypic.jpg"></div>',
)

# This raises AssertionError due to wrong src
assertInHTML(
    '<img src="otherpic.jpg" width=100>',
    '<div><img width=100 src="mypic.jpg"></div>',
)
