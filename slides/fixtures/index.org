* Fixtures
#+begin_notes
One of the key builtin features of pytest are fixtures. Fixtures are a way to
set up a testing environment in a minimalistic and reusable way.

When coming from the world of unittest, pytest fixtures do take some getting
used to, so I'll give a high-level overview of how pytest fixtures work.
#+end_notes

#+INCLUDE: includes/composition/index.org
#+INCLUDE: includes/scoping/index.org
#+INCLUDE: includes/overriding/index.org
#+INCLUDE: includes/arrange-act-assert/index.org
