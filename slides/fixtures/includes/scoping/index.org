** Scoping
#+begin_notes
To make our test setup more efficient and therefore faster, we can tell pytest
how often the value of a fixture will change. This is called the "scope" of the
fixture. For example, if the =expensive_thing= is essentially constant for the
entire test run, we can mark it with scope="session" and then pytest will
transparently cache the value for the entire test.

Possible values for scope are: function, class, module, package or session.
#+end_notes

#+INCLUDE: example.py src python
