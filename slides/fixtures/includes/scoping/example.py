@pytest.fixture(scope="session")
def expensive_thing():
    yield expensive_operation()

@pytest.fixture(scope="function")
def cheap_thing(expensive_thing):
    yield cheap_operation(expensive_thing)

@pytest.fixture(scope="function")
def other_cheap_thing(expensive_thing):
    yield other_cheap_operation(expensive_thing)

def test_cheap_thing(cheap_thing, other_cheap_thing):
    assert cheap_thing < other_cheap_thing
