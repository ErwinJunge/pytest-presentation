# root/conftest.py
@pytest.fixture
def backend():
    yield DefaultBackend()

# root/exact_online/conftest.py
@pytest.fixture
def backend():
    yield ExactOnlineBackend()

# root/afas/conftest.py
@pytest.fixture
def backend():
    yield AfasBackend()
