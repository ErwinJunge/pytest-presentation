@pytest.fixture
def expensive_thing():
    yield expensive_operation()

@pytest.fixture
def cheap_thing(expensive_thing):
    yield cheap_operation(expensive_thing)

@pytest.fixture
def other_cheap_thing(expensive_thing):
    yield other_cheap_operation(expensive_thing)

def test_cheap_thing(cheap_thing, other_cheap_thing):
    assert cheap_thing < other_cheap_thing
