** Composition
#+begin_notes
The major difference between the unittest way and the pytest way of doing setup
is in how to reuse code.

The way pytest fixtures are reused isn't through subclassing, but through
composition. When an argument of a test function contains the name of a fixture,
it's said to "request" that fixture. Fixtures themselves can also request other
fixtures and pytest will automatically resolve all the fixtures required to run
the test.
#+end_notes

#+INCLUDE: example.py src python
