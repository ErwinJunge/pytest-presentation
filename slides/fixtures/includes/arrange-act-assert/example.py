class TestHelloWorld:
    @pytest.fixture(scope="class")
    def hello(self):  # arrange
        yield "hello"

    @pytest.fixture(scope="class")
    def postfixed(self, hello):  # act
        yield postfix(hello)

    def test_postfix_hello(self, postfixed):  # assert
        assert "hello" in postfixed

    def test_postfix_world(self, postfixed):  # assert
        assert postfixed.endswith("world")
