** Arrange, act, assert
#+begin_notes
Using the scoping features of fixtures allows for more advanced use as well. You
can completely split up the arrange, act and assert phases. If "act" is an
expensive operation, this can speed up tests and still keep the assertions very
specific so that when a test fails it points at the specific issue and doesn't
hide other failures below it.
#+end_notes

#+INCLUDE: example.py src python
